import {select, Store} from '@ngrx/store';
import {FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {LoaderService} from '@sofisa/loader';
import {NotificationService} from '@sofisa/notification';
import {UpdateActionsOnTaskForm, formatDateToApiFormat, handleFieldChange} from '@sofisa/core';
import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
	selector: 'so-task-form',
	templateUrl: './task-form.component.html',
	styleUrls: ['./task-form.component.scss']
})

export class TaskFormComponent implements OnInit, OnChanges {
	handleFieldChange: any = handleFieldChange;
	store$: Observable<any>;
	@Input() props: {
		user: any,
		task: any,
		page: string,
		form: {
			formGroup: FormGroup,
			validations: any,
			actions: any
		}
	};
	@Output() submission: EventEmitter<FormGroup> = new EventEmitter();
	@Output() claim: EventEmitter<{ taskId: any, userId: any}> = new EventEmitter();
	
	constructor(public store: Store<{pages: any}>, public loaderService: LoaderService, public notificationService: NotificationService) {
		this.store$ = this.store.pipe(select('pages'));
	}
	
	ngOnInit() {
		this.store$.subscribe((store: any) => {
			this.props.form.validations = store.pages[this.props.page].form.validations;
			this.props.form.actions = store.pages[this.props.page].form.actions;
		});
	}
	
	ngOnChanges() {
		this.props.form.formGroup.updateValueAndValidity();
	}
	
	handleClaim() {
		this.claim.emit({
			taskId: this.props.task.id,
			userId: this.props.user.id,
		});
	}
	
	handleSubmission() {
		this.store.dispatch(new UpdateActionsOnTaskForm({
			page: this.props.page,
			actionName: 'submitted',
			actionValue: true
		}));
		
		if (this.props.page === 'tasklist/editar') {
			if (this.props.task.type === 'credito') {
				this.props.form.formGroup.get('variables.aprovadoT3.value').setValue('sim');
			} else if (this.props.task.type === 'comercial') {
				this.props.form.formGroup.get('variables.aprovadoT4.value').setValue('sim');
			}
		}
		
		if (this.props.form.formGroup.valid) {
			this.props.form.formGroup.get('variables.dataVisita.value').setValue(formatDateToApiFormat(this.props.form.formGroup.value.variables.dataVisita.value));
			
			this.submission.emit(this.props.form.formGroup);
		}
	}
}
