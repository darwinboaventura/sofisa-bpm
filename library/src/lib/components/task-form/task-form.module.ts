import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TaskFormComponent} from './task-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {InputModule, ButtonModule} from '@sofisa/ui';

@NgModule({
	declarations: [TaskFormComponent],
	imports: [
		CommonModule,
		InputModule,
		ButtonModule,
		ReactiveFormsModule
	],
	exports: [TaskFormComponent]
})

export class TaskFormModule {}
