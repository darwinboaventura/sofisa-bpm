import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DrawingComponent} from './drawing.component';

@NgModule({
	declarations: [DrawingComponent],
	imports: [
		CommonModule
	],
	exports: [DrawingComponent]
})

export class DrawingModule {}
