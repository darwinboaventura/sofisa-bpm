import BpmnJS from 'bpmn-js';
import {BpmService} from '../../services/bpm/bpm.service';
import {Component, Input, OnInit} from '@angular/core';

@Component({
	selector: 'so-drawing',
	templateUrl: './drawing.component.html',
	styleUrls: ['./drawing.component.scss']
})

export class DrawingComponent implements OnInit {
	@Input() width: any;
	@Input() height: any;
	@Input() diagram: any;
	
	public viewer: any;
	
	constructor(public bpmService: BpmService) {}
	
	ngOnInit() {
		this.viewer = new BpmnJS({
			container: '#viewer',
			width: this.width,
			height: this.height
		});
		
		this.viewer.importXML(this.diagram, (err: any) => {
			if (err) {
				console.log('XML error:', err);
			}
		});
	}
}
