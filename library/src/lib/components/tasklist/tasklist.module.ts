import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TasklistComponent} from './tasklist.component';

@NgModule({
	declarations: [TasklistComponent],
	imports: [
		CommonModule,
		RouterModule
	],
	exports: [TasklistComponent]
})

export class TasklistModule {}
