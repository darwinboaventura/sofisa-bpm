import {Component, Input, OnInit} from '@angular/core';

@Component({
	selector: 'so-tasklist',
	templateUrl: './tasklist.component.html',
	styleUrls: ['./tasklist.component.scss']
})

export class TasklistComponent implements OnInit {
	@Input() props: {
		tasks: []
	};
	
	constructor() {}
	
	ngOnInit() {}
}
